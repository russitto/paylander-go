package controllers

import "github.com/revel/revel"

type App struct {
	*revel.Controller
}

func (c App) Index() revel.Result {
    if c.Session["FbId"] != "" {
        return c.Redirect("/app/calendar")
    }
    return c.Render()
}

func (c App) Calendar() revel.Result {
    return c.Render()
}
