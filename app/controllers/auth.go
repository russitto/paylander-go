package controllers

import "strconv"
import "net/http"
import "math/rand"
import "io/ioutil"
import "encoding/json"
import "github.com/revel/revel"

type Auth struct {
	*revel.Controller
}

type fbCbMess struct {
    Token     string `json:"access_token"`
    TokenType string `json:"token_type"`
    Expires   int    `json:"expires_in"`
}

type fbUser struct {
    Id    string `json:"id"`
    Email string `json:"email"`
}

type errorString struct {
    s string
}

func (e *errorString) Error() string {
    return e.s
}

func floatToString(input_num float64) string {
    // to convert a float number to a string
    return strconv.FormatFloat(input_num, 'f', 6, 64)
}

func (c Auth) Login() revel.Result {
    fbId := revel.Config.StringDefault("paylander.facebook.id"      , "")
    base := revel.Config.StringDefault("paylander.facebook.self_url", "http://localhost/")
    r := rand.New(rand.NewSource(99))

    state := floatToString(r.Float64())
    c.Session["fbstate"] = state
    url := "http://www.facebook.com/dialog/oauth" + "?client_id=" + fbId + "&state=" + state + "&scope=email&redirect_uri=" + base + "auth/logincb"
	return c.Redirect(url)
}

func (c Auth) Logincb() revel.Result {
    var code, state, state0 string
    fbId   := revel.Config.StringDefault("paylander.facebook.id"      , "")
    base   := revel.Config.StringDefault("paylander.facebook.self_url", "http://localhost/")
    secret := revel.Config.StringDefault("paylander.facebook.secret"  , "")

    state0 = c.Session["fbstate"]
    c.Session["fbstate"] = ""
    c.Params.Bind(&state, "state")
    c.Params.Bind(&code, "code")
    if state0 != state {
        return c.RenderError(&errorString{"Bad code CSRF!!!"})
    }
    url := "https://graph.facebook.com/v2.3/oauth/access_token?client_id=" + fbId + "&client_secret=" + secret + "&code=" + code + "&grant=client_credentials&redirect_uri=" + base + "auth/logincb"
    resp, err := http.Get(url)
    if err != nil {
        revel.ERROR.Printf("--------------------------------------------- %s", err)
    }
    defer resp.Body.Close()
    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        revel.ERROR.Printf("--------------------------------------------- %s", err)
    }
    var m fbCbMess
    err = json.Unmarshal(body, &m)
    if err != nil {
        revel.ERROR.Printf("--------------------------------------------- %s", err)
    }
    resp, err = http.Get(url)
    if err != nil {
        revel.ERROR.Printf("--------------------------------------------- %s", err)
    }
    url = "https://graph.facebook.com/v2.3/me?access_token=" + m.Token
    resp, err = http.Get(url)
    if err != nil {
        revel.ERROR.Printf("--------------------------------------------- %s", err)
    }
    defer resp.Body.Close()
    body, err = ioutil.ReadAll(resp.Body)
    revel.INFO.Printf("--------------------------------------------- %s", body)
    if err != nil {
        revel.ERROR.Printf("--------------------------------------------- %s", err)
    }
    var u fbUser
    err = json.Unmarshal(body, &u)
    if err != nil {
        revel.ERROR.Printf("--------------------------------------------- %s", err)
    }
    c.Session["FbId"] = u.Id
    c.Session["FbEmail"] = u.Email
    c.Flash.Success("Logged In")
    return c.Redirect("/")
}

func (c Auth) Logout() revel.Result {
    c.Session["FbId"] = ""
    c.Session["FbEmail"] = ""
	return c.Redirect("/")
}
