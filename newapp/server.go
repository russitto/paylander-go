package main

import (
    "github.com/gorilla/mux"
    "log"
    "net/http"
    "./controllers/Home"
)

func main() {
    r := mux.NewRouter()
    r.HandleFunc("/", controllers.Home.Index)
    http.Handle("/", r)
    log.Println("Listening...")
    http.ListenAndServe(":8080", nil)
}
