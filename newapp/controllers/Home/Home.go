package controllers

import (
    "net/http"
    "../Controller"
)

type Index struct {
	*controllers.Controller
}

func (c Index) Index(w http.ResponseWriter, r *http.Request)  {
    w.Write([]byte("Hola mundo"))
}
